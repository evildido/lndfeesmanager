package main

import (
	"io/ioutil"
	"os"
	"time"

	"gitlab.com/nolith/lndfeesmanager/strategies"

	"github.com/pelletier/go-toml"
)

type Fee struct {
	BaseMsat      int64
	Rate          float64
	TimeLockDelta uint32
}

type BalanceStrategy struct {
	LocalUnbalancedLimit  int64
	RemoteUnbalancedLimit int64

	LocalUnbalancedFee  Fee
	RemoteUnbalancedFee Fee
	BalancedFee         Fee
}

type IgnoreStrategy struct {
	Channels []uint64
}

type Lnd struct {
	Address string
	Network Network
}

type Network string

var (
	Mainnet = Network("mainnet")
	Testnet = Network("testnet")
)

type Config struct {
	Verbose         bool
	Lnd             Lnd
	BalanceStrategy BalanceStrategy
	IgnoreStrategy  IgnoreStrategy
	Daemon          bool
	Interval        time.Duration
}

func defaultConfig() *Config {
	return &Config{
		Verbose:  false,
		Daemon:   false,
		Interval: 1 * time.Minute,
		Lnd: Lnd{
			Address: defaultAddress,
			Network: Mainnet,
		},
		BalanceStrategy: BalanceStrategy{
			LocalUnbalancedLimit:  defaultLocalUnbalancedFeesLimit,
			RemoteUnbalancedLimit: defaultRemoteUnbalancedFeesLimit,

			LocalUnbalancedFee: Fee{
				BaseMsat:      0,
				Rate:          0.0001,
				TimeLockDelta: defaultTimeLockDelta,
			},

			BalancedFee: Fee{
				BaseMsat:      1000,
				Rate:          0.0002,
				TimeLockDelta: defaultTimeLockDelta,
			},

			RemoteUnbalancedFee: Fee{
				BaseMsat:      1000,
				Rate:          0.0005,
				TimeLockDelta: defaultTimeLockDelta,
			},
		},
	}
}

func LoadConfig(path string) (*Config, error) {
	config := defaultConfig()
	doc, err := ioutil.ReadFile(path)
	if err != nil {
		if os.IsNotExist(err) {
			return config, nil
		}
		return nil, err
	}

	err = toml.Unmarshal(doc, config)

	return config, err
}

func getBalanceStrategy(strategy BalanceStrategy) *strategies.Balance {
	return &strategies.Balance{
		LocalUnbalancedLimit:  strategy.LocalUnbalancedLimit,
		RemoteUnbalancedLimit: strategy.RemoteUnbalancedLimit,

		LocalUnbalancedFee:  feesToPolicy(strategy.LocalUnbalancedFee),
		RemoteUnbalancedFee: feesToPolicy(strategy.RemoteUnbalancedFee),
		BalancedFee:         feesToPolicy(strategy.BalancedFee),
	}
}

func feesToPolicy(fee Fee) strategies.Policy {
	return strategies.Policy{
		BaseFeeMsat:   fee.BaseMsat,
		FeeRate:       fee.Rate,
		TimeLockDelta: fee.TimeLockDelta,
	}
}
