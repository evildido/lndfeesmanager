module gitlab.com/nolith/lndfeesmanager

go 1.14

require (
	github.com/apex/log v1.9.0
	github.com/golang/protobuf v1.4.2 // indirect
	github.com/lightningnetwork/lnd v0.10.4-beta
	github.com/pelletier/go-toml v1.8.0
	google.golang.org/genproto v0.0.0-20200624020401-64a14ca9d1ad // indirect
	google.golang.org/grpc v1.31.0
)
