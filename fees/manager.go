package fees

import (
	"context"
	"encoding/hex"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"os/signal"
	"path"
	"runtime"
	"strconv"
	"strings"
	"syscall"
	"time"

	"gitlab.com/nolith/lndfeesmanager/strategies"

	"github.com/apex/log"
	"github.com/lightningnetwork/lnd/lnrpc"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/metadata"
)

type Manager struct {
	lnd lnrpc.LightningClient
	ctx context.Context

	strategies strategies.Factory

	connection io.Closer
}

func NewManager(address, network string, strategiesFactory *strategies.Factory) (*Manager, error) {
	confDir, err := getLndConfigDir()
	if err != nil {
		return nil, fmt.Errorf("Cannot detect LND config directory: %v", err)
	}

	tc, err := credentials.NewClientTLSFromFile(path.Join(confDir, "tls.cert"), "")
	if err != nil {
		return nil, fmt.Errorf("cannot load credentials: %v", err)
	}

	macaroonPath := path.Join(confDir, "data", "chain", "bitcoin", network, "admin.macaroon")
	m, err := ioutil.ReadFile(macaroonPath)
	if err != nil {
		return nil, fmt.Errorf("cannot load admin macaroon: %v", err)
	}

	macaroon := hex.EncodeToString(m)
	authCtx := metadata.AppendToOutgoingContext(context.Background(), "macaroon", macaroon)

	// Set up a connection to the server.
	conn, err := grpc.Dial(address, grpc.WithTransportCredentials(tc), grpc.WithBlock())
	if err != nil {
		return nil, fmt.Errorf("cannot connect: %v", err)
	}

	return &Manager{
		lnd:        lnrpc.NewLightningClient(conn),
		ctx:        authCtx,
		strategies: *strategiesFactory,
		connection: conn,
	}, nil
}

func (m *Manager) Close() error {
	return m.connection.Close()
}

func (m *Manager) Run(reckless bool, daemon bool, interval time.Duration) error {
	log.WithFields(log.Fields{
		"reckless": reckless,
		"daemon":   daemon,
		"interval": interval,
	}).Info("Start")

	run := func() error {
		err := m.checkChannels(reckless)
		if err != nil {
			log.WithError(err).Warn("Something went wrong")
		}

		return err
	}

	err := run()
	if !daemon {
		return err
	}

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	for {
		select {
		case sig := <-sigs:
			log.WithField("signal", sig).Warn("Signal received")
			return nil

		case <-time.After(interval):
			run()
		}
	}
}

func (m *Manager) checkChannels(reckless bool) error {
	chanCtx, chanCancel := context.WithTimeout(m.ctx, 2*time.Second)
	defer chanCancel()

	chans, err := m.lnd.ListChannels(chanCtx, &lnrpc.ListChannelsRequest{})
	if err != nil {
		return fmt.Errorf("could not get channels: %v", err)
	}

	var failure bool
	for _, c := range chans.GetChannels() {
		err := m.checkChannel(c, reckless)
		if err != nil {
			failure = true
		}
	}

	if failure {
		return fmt.Errorf("Some channel failed")
	}

	return nil
}

func (m *Manager) checkChannel(c *lnrpc.Channel, reckless bool) error {
	nodeInfoCtx, nodeInfopCancel := context.WithTimeout(m.ctx, 500*time.Millisecond)
	defer nodeInfopCancel()
	logger := log.WithFields(log.Fields{
		"channel": c.ChanId,
	})

	nodeInfo, err := m.lnd.GetNodeInfo(nodeInfoCtx, &lnrpc.NodeInfoRequest{PubKey: c.RemotePubkey})
	if err != nil {
		logger.WithField("remote_pubkey", c.RemotePubkey).WithError(err).Error("Cannot get node info")
		logger.WithError(err).Error("Cannot update channel policy")

		return fmt.Errorf("cannot update channel policy")
	}

	chanInfoCtx, chanInfoCancel := context.WithTimeout(m.ctx, 500*time.Millisecond)
	defer chanInfoCancel()

	chanInfo, err := m.lnd.GetChanInfo(chanInfoCtx, &lnrpc.ChanInfoRequest{ChanId: c.ChanId})
	if err != nil {
		logger.WithError(err).Error("Cannot get channel info")
	}

	balancePercentage := localbalancePercentage(c)

	logger = logger.WithFields(log.Fields{
		"alias":              nodeInfo.Node.Alias,
		"balance_percentage": balancePercentage,
	})
	logger.WithFields(log.Fields{
		"local_balance":  c.LocalBalance,
		"remote_balance": c.RemoteBalance,
		"chan_point":     chanInfo.ChanPoint,
	}).Debug("Checking balance percentage")

	var local, remote *lnrpc.RoutingPolicy
	if chanInfo.Node1Pub == c.RemotePubkey {
		remote = chanInfo.Node1Policy
		local = chanInfo.Node2Policy
	} else {
		remote = chanInfo.Node2Policy
		local = chanInfo.Node1Policy
	}

	logger.WithFields(log.Fields{
		"fee_base_msat":   local.FeeBaseMsat,
		"fee_rate":        local.FeeRateMilliMsat,
		"time_lock_delta": local.TimeLockDelta,
	}).Debug("local policy")
	logger.WithFields(log.Fields{
		"fee_base_msat":   remote.FeeBaseMsat,
		"fee_rate":        remote.FeeRateMilliMsat,
		"time_lock_delta": remote.TimeLockDelta,
	}).Debug("remote policy")

	policy := m.generateNewPolicy(c, local, remote, logger)

	if policy == nil {
		logger.Debug("Nothing to change")

		return nil
	}

	policy.Scope = &lnrpc.PolicyUpdateRequest_ChanPoint{
		ChanPoint: getChanPont(chanInfo.ChanPoint),
	}

	logger.WithFields(log.Fields{
		"fee_base_msat":   policy.BaseFeeMsat,
		"fee_rate":        int64(policy.FeeRate * 1000000),
		"time_lock_delta": policy.TimeLockDelta,
	}).Info("new policy")

	if !reckless {
		return nil
	}

	updateCtx, updateCancel := context.WithTimeout(m.ctx, 500*time.Millisecond)
	defer updateCancel()

	_, err = m.lnd.UpdateChannelPolicy(updateCtx, policy)
	if err != nil {
		logger.WithError(err).Error("Cannot update channel policy")

		return fmt.Errorf("cannot update channel policy")
	}

	return nil
}

func localbalancePercentage(c *lnrpc.Channel) int64 {
	return c.LocalBalance * 100 / (c.Capacity - c.LocalChanReserveSat - c.RemoteChanReserveSat)
}

func (m *Manager) generateNewPolicy(c *lnrpc.Channel, local, remote *lnrpc.RoutingPolicy, _ log.Interface) *lnrpc.PolicyUpdateRequest {
	strategy := m.strategies.Get(c.ChanId)

	policy := strategy.Apply(c, local, remote)
	if policy == nil {
		return nil
	}

	return &lnrpc.PolicyUpdateRequest{
		TimeLockDelta: policy.TimeLockDelta,
		BaseFeeMsat:   policy.BaseFeeMsat,
		FeeRate:       policy.FeeRate,
	}
}

func (m *Manager) CheckLndConnection() error {
	ctx2, cancel := context.WithTimeout(m.ctx, 500*time.Millisecond)
	defer cancel()

	i, err := m.lnd.GetInfo(ctx2, &lnrpc.GetInfoRequest{})
	if err != nil {
		log.Fatalf("could not get info: %v", err)
		return err
	}
	log.WithFields(log.Fields{
		"alias":           i.Alias,
		"active_channels": i.NumActiveChannels,
		"peers":           i.NumPeers,
	}).Info("lnd connection")

	return nil
}

func getChanPont(chanpoint string) *lnrpc.ChannelPoint {
	split := strings.SplitN(chanpoint, ":", 2)
	txId := split[0]
	outIdx, err := strconv.ParseUint(split[1], 10, 32)
	if err != nil {
		log.WithError(err).Fatal("Cannot split chanpoint")
	}

	return &lnrpc.ChannelPoint{
		FundingTxid: &lnrpc.ChannelPoint_FundingTxidStr{FundingTxidStr: txId},
		OutputIndex: (uint32)(outIdx),
	}
}

func getLndConfigDir() (string, error) {
	if runtime.GOOS == "linux" {
		home, err := os.UserHomeDir()
		if err != nil {
			return "", err
		}

		return path.Join(home, ".lnd"), nil
	} else {
		confDir, err := os.UserConfigDir()
		if err != nil {
			return "", err
		}

		return path.Join(confDir, "Lnd"), nil
	}
}
