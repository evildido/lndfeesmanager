# LND fees manager

This software is higly experimetal! **Use it at your own risk**

`lndfeesmanager` is a siple software that aims to balance LN channels using a dynamic fees.

When a channel is unbalanced it will update the fees.

* If the balance is mostly local, fees will be decresed to incentivate routing.
* If the balance is mostly remote, fees will be increased to disincentivate routing.
* If the channel is balanced, a medium fee will be set.

## How to run

1. compile it with `go get gitlab.com/nolith/lndfeesmanager`
1. run `lndfeesmanager -v`, by default it runs un dry-run mode, so it will only print the new fees without changing anything
1. prepare your configuration file
1. run as a daemon `lndfeesmanager -d --rackless`

## Configuration

The configuration file is located at `~/.config/lndfeesmanager/config.toml`, please take a look at [the example config](https://gitlab.com/nolith/lndfeesmanager/-/blob/master/config.example.toml) and adapt it to your needs.

## Know limitations

It can reach a remote `lnd` with some effort, but it expects to find the lnd configuration on the local disk.

## Tip me

If you like this project and might even use or extend it, why not [send some tip love](https://thanks.abisso.org/paywall/dWB3veCAK56W8AsP3tRQ9e)!